class Cleaner {
  final String name;
  final String id;
  final String status;

  Cleaner({required this.name, required this.id, required this.status});

  Map<String, dynamic> toJson() => {
    'name': name,
    'id': id,
    'status': status,
  };

  factory Cleaner.fromJson(Map<String, dynamic> json) => Cleaner(
    name: json['name'],
    id: json['id'],
    status: json['id'],
  );
}
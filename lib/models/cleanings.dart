class Cleaning {
  final int? id;
  final DateTime cleaningEndDate;
  final String cleanerId;
  final String cleaningType;
  bool earlyStopped;

  Cleaning(
      {required this.cleanerId,
      required this.cleaningEndDate,
      this.id,
      this.earlyStopped = false,
      required this.cleaningType});

  Map<String, dynamic> toJson() => {
        'id': id,
        'cleaner_id': cleanerId,
        'cleaning_end_date': cleaningEndDate.toIso8601String(),
        'early_stopped': earlyStopped,
        'cleaning_type': cleaningType
      };

  factory Cleaning.fromJson(Map<String, dynamic> json) => Cleaning(
      cleanerId: json['cleaner_id'],
      cleaningEndDate: DateTime.parse(json['cleaning_end_date']),
      earlyStopped: json['early_stopped'],
      id: json['id'],
      cleaningType: json['cleaning_type']);
}

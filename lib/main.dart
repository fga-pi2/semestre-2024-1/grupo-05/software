import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:solar_panel_cleaner/pages/HomePage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Solar Panel Cleaner',
      theme: ThemeData(
          fontFamily: 'roboto',
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.lightBlue,
            backgroundColor: const Color(0xfff0f0f7),
          ),
          primaryColor: Colors.lightBlue,
          useMaterial3: true),
      home: const HomePage(title: 'Limpador de Placa Solar'),
      debugShowCheckedModeBanner: false,
    );
  }
}

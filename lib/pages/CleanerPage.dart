import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:solar_panel_cleaner/controllers/cleaner_page_controller.dart';
import 'package:solar_panel_cleaner/utils/constants.dart';
import '../models/cleaner.dart';

class CleanerPage extends StatefulWidget {
  final Cleaner cleaner;

  const CleanerPage({super.key, required this.cleaner});

  @override
  State<CleanerPage> createState() => _CleanerPageState();
}

class _CleanerPageState extends State<CleanerPage> {
  late CleanerPageController controller;

  @override
  void initState() {
    controller = CleanerPageController(cleanerId: widget.cleaner.id);
    controller.init();
    super.initState();
  }

  void _startCleaning() {
    controller.startCleaning();
  }

  void _requestCleaning() {
    controller.requestCleaning();
  }

  void _stopCleaning() {
    controller.stopCleaning();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  String formatDuration(RxInt seconds) {
    final hours = seconds ~/ 3600;
    final minutes = (seconds % 3600) ~/ 60;
    final remainingSeconds = seconds % 60;

    return '${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}:${remainingSeconds.toString().padLeft(2, '0')}';
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text(
          widget.cleaner.name,
          style: const TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Obx(() {
          IconData icon;
          Color color;
          String message;

          switch (controller.connectionState.value) {
            case 'connected':
              icon = Icons.check_circle;
              color = Colors.green;
              message = 'Conectado ao limpador com sucesso.';
              break;
            case 'failed':
              icon = Icons.error;
              color = Colors.red;
              message =
                  'Falha ao conectar com o limpador, tente novamente. Caso o erro persista, é necessário manutenção no limpador.';
              break;
            case 'connecting':
            default:
              icon = Icons.autorenew;
              color = Colors.blue;
              message = 'Estabelecendo conexão com o limpador.';
          }

          return Skeletonizer(
            enabled: controller.state.value == 'isLoading',
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text('Modos de Limpeza',
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ListTile(
                  title: const Text(
                    'Seco',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  subtitle: const Text(
                      'Limpeza utilizando métodos secos, como escovas ou panos, sem uso de água direta. O motor percorre a placa e gira para remover poeira e sujeira.'),
                  leading: Radio<String>(
                    value: 'dry',
                    groupValue: controller.cleaningType.value,
                    onChanged: (String? value) =>
                        controller.setCleaningType(value!),
                  ),
                ),
                ListTile(
                  title: const Text(
                    'Com água',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  subtitle: const Text(
                      'Limpeza utilizando água. O motor percorre a placa e gira enquanto uma válvula de água irriga para remover sujeiras incrustadas.'),
                  leading: Radio<String>(
                    value: 'wet',
                    groupValue: controller.cleaningType.value,
                    onChanged: (String? value) =>
                        controller.setCleaningType(value!),
                  ),
                ),
                const SizedBox(height: 20),
                if (controller.state.value == 'isCleaning')
                  LinearProgressIndicator(
                    value:
                        (controller.cleaningCounter.value / CLEAN_TIME_SECONDS)
                            .clamp(0.0, 1.0),
                    backgroundColor: Colors.grey[200],
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).primaryColor),
                  ),
                if (controller.state.value == 'isCleaning')
                  Align(
                    alignment: Alignment.centerLeft, //
                    child: SizedBox(
                      height: size.height * 0.1,
                      child: Text(
                          controller.cleaningCounter.value > 0
                              ? 'Tempo de limpeza estimado restante: ${formatDuration(controller.cleaningCounter)}'
                              : 'Tempo de limpeza excedido. Verifique o limpador.',
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                  )
                else
                  SizedBox(
                    height: size.height * 0.1,
                  ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(width: 28),
                      controller.connectionState.value == 'connecting'
                          ? buildCircularLoadingWidget()
                          : Icon(icon, color: color),
                      const SizedBox(width: 18),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            message,
                            maxLines: 10,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.1),
                Center(
                  child: controller.state.value != 'requested_cleaning' &&
                          controller.state.value != 'requested_stop'
                      ? SizedBox(
                          width: size.width * 0.5,
                          height: size.height * 0.065,
                          child: Theme(
                            data: ThemeData(useMaterial3: false),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor:
                                      controller.state.value == 'isCleaning'
                                          ? Colors.red.shade400
                                          : Theme.of(context).primaryColor),
                              onPressed: controller.connectionState.value !=
                                      'connected'
                                  ? null
                                  : controller.state.value == 'isCleaning'
                                      ? _stopCleaning
                                      : _requestCleaning,
                              child: Text(controller.state.value == 'isCleaning'
                                  ? 'Parar Limpeza'
                                  : 'Iniciar Limpeza'),
                            ),
                          ),
                        )
                      : buildCircularLoadingWidget(),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget buildCircularLoadingWidget() {
    return const CircularProgressIndicator();
  }
}

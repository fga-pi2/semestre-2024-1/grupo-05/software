import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../models/cleaner.dart';
import '../controllers/cleaner_service.dart';
import './CleanerPage.dart';
import '../utils/input_sanitization.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Cleaner> cleaners = [];
  CleanerService cleanerService = CleanerService();

  @override
  void initState() {
    super.initState();
    _loadCleaners();
  }

  Future<void> _loadCleaners() async {
    List<Cleaner> loadedCleaners = await cleanerService.cleaners();
    setState(() {
      cleaners = loadedCleaners;
    });
  }

  void _addCleaner(String name, String id, String status) {
    String? validationMessage = validateInputs(name, id, cleaners);
    if (validationMessage != null) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(validationMessage)));
      return;
    }

    setState(() {
      cleaners.add(Cleaner(name: name, id: id, status: status));
      cleanerService.insertCleaner(Cleaner(name: name, id: id, status: status));
    });
  }

  void _editCleaner(int index) {
    final _nameController = TextEditingController(text: cleaners[index].name);
    final _idController = TextEditingController(text: cleaners[index].id);
    final _status = cleaners[index].status;

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Editar Limpador'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: _nameController,
                decoration: const InputDecoration(labelText: 'Nome'),
              ),
              TextField(
                controller: _idController,
                decoration: const InputDecoration(labelText: 'ID'),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                String? validationMessage = validateInputs(_nameController.text, _idController.text, cleaners, index: index);
                if (validationMessage != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(validationMessage)));
                  return;
                }


                setState(() {
                  cleaners[index] = Cleaner(
                      name: _nameController.text,
                      id: _idController.text,
                      status: _status);
                  cleanerService.updateCleaner(Cleaner(
                      name: _nameController.text,
                      id: _idController.text,
                      status: _status));
                });
                Navigator.pop(context);
              },
              child: const Text('Salvar'),
            ),
          ],
        );
      },
    );
  }

  void _deleteCleaner(int index) {
    setState(() {
      cleanerService.deleteCleaner(cleaners[index].name);
      cleaners.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Text(
            widget.title,
            style: const TextStyle(color: Colors.white),
          ),
        ),
        centerTitle: true,
        leading: Padding(
          padding: const EdgeInsets.all(10.0), // Adjust the padding as needed
          child: SvgPicture.asset(
            'assets/solar_panel.svg',
            color: Colors.white, // Set color if the SVG should be colored
          ),
        ),
      ),
      body: buildListView(size),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () {
          final nameController = TextEditingController();
          final idController = TextEditingController();
          const status = 'Desligado';
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Adicionar Limpador'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: nameController,
                      decoration: const InputDecoration(labelText: 'Nome'),
                    ),
                    TextField(
                      controller: idController,
                      decoration: const InputDecoration(labelText: 'ID'),
                    ),
                  ],
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      _addCleaner(
                          nameController.text, idController.text, status);
                      Navigator.pop(context);
                    },
                    child: const Text('Adicionar'),
                  ),
                ],
              );
            },
          );
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget buildListView(size) {
    return cleaners.isNotEmpty
        ? ListView.builder(
            itemCount: cleaners.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                child: InkWell(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Icon(
                              Icons.sunny,
                              color: Colors.orange.shade300,
                              size: size.width * 0.08,
                            ),
                            SizedBox(
                              width: size.width * 0.1,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  cleaners[index].name,
                                  style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      bottom: size.height * 0.01,
                                      top: size.height * 0.02),
                                  child: Text(
                                    'ID: ${cleaners[index].id}',
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey),
                                  ),
                                )
                              ],
                            ),
                            const Spacer(),
                            IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () {
                                _editCleaner(index);
                              },
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red.shade400,
                              ),
                              onPressed: () {
                                _deleteCleaner(index);
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            CleanerPage(cleaner: cleaners[index]),
                      ),
                    );
                  },
                ),
              );
            },
          )
        : SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      bottom: size.height * 0.1, top: size.height * 0.2),
                  child: Center(
                    child: Text(
                      "Não há nada aqui ainda!",
                      style: TextStyle(fontSize: size.height * 0.028),
                    ),
                  ),
                ),
                SizedBox(
                    height: size.height * 0.2,
                    child: Image.asset('assets/add_file.png')),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: size.height * 0.05),
                  child: Center(
                    child: Text(
                      "Por favor, cadastre um novo limpador.",
                      style: TextStyle(fontSize: size.height * 0.021),
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}

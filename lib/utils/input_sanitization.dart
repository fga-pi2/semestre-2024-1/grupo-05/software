// Adicionar funções para tratar input nesse arquivo

import '../models/cleaner.dart';

String? validateInputs(String name, String id, List<Cleaner> cleaners, {int? index}) {
  if (name.isEmpty || id.isEmpty) {
    return 'Nome ou ID não podem estar vazios';
  }

    for (int i = 0; i < cleaners.length; i++) {
    if (i != index && (cleaners[i].name == name || cleaners[i].id == id)) {
      return 'Nome ou ID já existem';
    }
  }
  
  return null;
}

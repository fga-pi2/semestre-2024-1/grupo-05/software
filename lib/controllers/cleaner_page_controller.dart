import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/get_rx.dart';
import 'package:solar_panel_cleaner/controllers/cleaner_service.dart';
import 'package:solar_panel_cleaner/models/cleanings.dart';
import 'package:solar_panel_cleaner/mqtt_service/mqtt_service.dart';
import 'package:solar_panel_cleaner/utils/constants.dart';

class CleanerPageController {
  String cleanerId;
  Cleaning? currentCleaning;
  RxString state = 'isLoading'.obs;
  RxString cleaningType = 'dry'.obs;
  RxString connectionState = 'connecting'.obs;
  Timer? _timer;
  Timer? _timerSign;
  late Timer _connectionTimer;
  RxInt cleaningCounter = 0.obs;
  MqttService mqttService;
  CleanerService cleanerService;
  bool sign = false;

  CleanerPageController({required this.cleanerId})
      : mqttService = MqttService(cleanerId),
        cleanerService = CleanerService();

  requestCleaning() async {
    mqttService.sendMessage(cleaningType.value);
    state.value = 'requested_cleaning';
  }

  requestStop() async {
    mqttService.sendMessage('stop');
    state.value = 'requested_stop';
  }

  startCleaning() async {
    if (state.value == 'isCleaning') {
      Get.snackbar('Erro', 'Uma limpeza já está em andamento.',
          margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));
      return;
    }

    state.value = 'isCleaning';

    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      cleaningCounter.value -= 1;
    });

    currentCleaning = Cleaning(
        cleanerId: cleanerId,
        cleaningEndDate:
            DateTime.now().add(const Duration(seconds: CLEAN_TIME_SECONDS)),
        cleaningType: cleaningType.value);

    cleaningCounter.value = CLEAN_TIME_SECONDS;

    await cleanerService.insertCleaning(currentCleaning!);

    Get.snackbar('Limpeza Iniciada',
        'Você pode encerrar manualmente a qualquer momento.',
        margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));

    // mqttService.sendMessage(cleaningType.value);
  }

  void init() async {
    _connectionTimer = Timer(const Duration(seconds: 5), () {
      if (connectionState.value != 'connected') {
        connectionState.value = 'failed';
      }

      _connectionTimer.cancel();
    });

    mqttService.connect(handleEspMessages);
    List<Cleaning> cleanings = await cleanerService.getLastCleaning(cleanerId);

    _timerSign = Timer.periodic(const Duration(seconds: 5), (timer) {
      if (sign == false) {
        if (connectionState.value != 'failed') {
          Get.snackbar('Erro na conexão',
              'Não foi possível estabelecer conexão com a placa.',
              margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));
        }
        connectionState.value = 'failed';
      }
      if (sign == true) {
        if (connectionState.value != 'connected') {
          Get.snackbar('Reconectado', 'A aplicação foi reconectada ao limpador',
              margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));
        }
        connectionState.value = 'connected';
      }
      sign = false;
    });

    if (cleanings.isEmpty) {
      state.value = 'idle';
      return;
    }

    final lastCleaning = cleanings[0];

    final bool noActiveCleaning =
        !lastCleaning.cleaningEndDate.isAfter(DateTime.now());

    if (noActiveCleaning) {
      state.value = 'idle';
      return;
    }

    if (_timer != null) {
      _timer!.cancel();
    }

    state.value = 'isCleaning';

    cleaningType.value = lastCleaning.cleaningType;

    currentCleaning = lastCleaning;

    currentCleaning = lastCleaning;
    cleaningCounter.value =
        lastCleaning.cleaningEndDate.difference(DateTime.now()).inSeconds;

    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (cleaningCounter.value > 0) {
        cleaningCounter.value -= 1;
      }
    });

    // mqttService.listenToUpdates(handleEspMessages);
  }

  void handleEspMessages(String message) async {
    if (message == 'finalizou') {
      connectionState.value = 'connected';
      sign = true;

      if (_timer != null) {
        _timer!.cancel();
        _timer = null;
      }

      if (state.value == 'isCleaning') {
        state.value = 'idle';

        Get.snackbar(
            'Limpeza Finalizada', 'O limpador finalizou o seu percurso',
            margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));

        currentCleaning!.earlyStopped = true;

        await cleanerService.updateCleaning(currentCleaning!);
      }
    } else if (message == 'nao-finalizou') {
      sign = true;
      connectionState.value = 'connected';
    } else if (message == 'iniciou-limpeza') {
      startCleaning();
    }
  }

  void stopCleaning() async {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }

    state.value = 'idle';

    mqttService.sendMessage('stop');

    Get.snackbar('Limpeza Finalizada', '',
        margin: const EdgeInsets.fromLTRB(70, 0, 70, 0));

    currentCleaning!.earlyStopped = true;
    await cleanerService.updateCleaning(currentCleaning!);
  }

  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }

    if (_timerSign != null) {
      _timerSign!.cancel();
      _timerSign = null;
    }
  }

  void setCleaningType(String type) {
    cleaningType.value = type;
  }
}

import 'package:solar_panel_cleaner/models/cleanings.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import '../models/cleaner.dart';

class CleanerService {
  Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    // Initialize the database
    _database = await initDB();
    return _database!;
  }

  Future<Database> initDB() async {
    String path = join(await getDatabasesPath(), 'cleaner_database.db');
    return await openDatabase(
      path,
      onCreate: (db, version) async {
        await db.execute('''
        CREATE TABLE cleaners (
            id TEXT PRIMARY KEY,
            name TEXT UNIQUE,
            status TEXT CHECK (status IN ('Ligado', 'Desligado'))
        );
        ''');

        await db.execute('''
         CREATE TABLE cleanings (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            cleaner_id TEXT,
            cleaning_end_date DATE,
            early_stopped BOOLEAN DEFAULT FALSE,
            cleaning_type TEXT CHECK(cleaning_type IN ('dry', 'wet')),
            FOREIGN KEY (cleaner_id) REFERENCES cleaners(id)
        );
        ''');
      },
      version: 2,
    );
  }

  Future<void> insertCleaner(Cleaner cleaner) async {
    final db = await database;

    await db.insert(
      'cleaners',
      cleaner.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertCleaning(Cleaning cleaning) async {
    final db = await database;

    await db.insert(
      'cleanings',
      cleaning.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Cleaning>> getLastCleaning(String cleanerId) async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('cleanings',
        where: 'cleaner_id = ? AND early_stopped = false',
        whereArgs: [cleanerId],
        orderBy: 'cleaning_end_date DESC');

    return List.generate(maps.length, (i) {
      return Cleaning(
        cleanerId: maps[i]['cleaner_id'],
        id: maps[i]['id'],
        earlyStopped: maps[i]['early_stopped'] != 0,
        cleaningEndDate: DateTime.parse(maps[i]['cleaning_end_date']),
        cleaningType: maps[i]['cleaning_type'],
      );
    });
  }

  Future<List<Cleaner>> cleaners() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('cleaners');

    return List.generate(maps.length, (i) {
      return Cleaner(
        name: maps[i]['name'],
        id: maps[i]['id'],
        status: maps[i]['status'],
      );
    });
  }

  Future<void> updateCleaner(Cleaner cleaner) async {
    final db = await database;

    await db.update(
      'cleaners',
      cleaner.toJson(),
      where: "name = ?",
      whereArgs: [cleaner.name],
    );
  }

  Future<void> updateCleaning(Cleaning cleaninig) async {
    final db = await database;

    await db.update(
      'cleanings',
      cleaninig.toJson(),
      where: "id = ?",
      whereArgs: [cleaninig.id],
    );
  }

  Future<void> deleteCleaner(String name) async {
    final db = await database;

    await db.delete(
      'cleaners',
      where: "name = ?",
      whereArgs: [name],
    );
  }
}

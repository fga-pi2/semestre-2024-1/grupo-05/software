# App grupo 4

Este é um projeto Flutter que pode ser executado em múltiplas plataformas, incluindo Android, iOS, Windows, macOS, e Linux.

## Pré-requisitos

Certifique-se de ter os seguintes itens instalados em seu sistema:

- [Flutter SDK](https://flutter.dev/docs/get-started/install)
- [Android Studio](https://developer.android.com/studio) (com Android SDK)
- [Xcode](https://developer.apple.com/xcode/) (para desenvolvimento iOS)
- Um editor de código (recomenda-se [Visual Studio Code](https://code.visualstudio.com/))
- [Git](https://git-scm.com/)

## Configuração Inicial

1. **Clone o repositório:**

   ```sh
   git clone https://github.com/seu-usuario/seu-projeto.git
   cd seu-projeto

2. Instale as dependências do Flutter:

    ```sh
    flutter pub get

3. Verifique a configuração do Flutter:

    Execute o seguinte comando para verificar se todas as dependências do seu ambiente estão configuradas corretamente.

        flutter doctor

Siga as instruções fornecidas pelo flutter doctor para corrigir quaisquer problemas.

## Executando o Projeto

Android ou iOS
Inicie um emulador Android ou conecte um dispositivo Android via USB.

Execute o projeto:

``sh
flutter run
